module server

go 1.21.4

replace github.com/gocql/gocql => github.com/scylladb/gocql v1.12.0

require github.com/go-chi/chi/v5 v5.0.10 // indirect
