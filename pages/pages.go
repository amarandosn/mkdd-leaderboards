package pages

import (
	"fmt"
	"log"
	"os"
	"net/http"
	"html/template"

	"github.com/go-chi/chi/v5"
)

const pageDir = "pages/src"

type Template struct {
	Page template.HTML
}

func handleError(err error) {
	if err != nil {
		log.Println(err)
	}
}

func getComp(name string) string {
	return fmt.Sprintf("%v/%v.html", pageDir, name)
}

func getPageFromRequest(r *http.Request) string {
	path := r.URL.Path
	if path == "/" {
		return fmt.Sprintf("%v/index.html", pageDir)
	}
	return fmt.Sprintf("%v%v.html", pageDir, r.URL.Path)
}

func Render(w http.ResponseWriter, r *http.Request) {
	hxRequest := r.Header["Hx-Request"]
	if len(hxRequest) > 0 && hxRequest[0] == "true" { 
		w.Header().Add("HX-Replace-Url", r.URL.Path)
		// http.ServeFile(w, r, getComp(r.URL.Path[1:]))
		http.ServeFile(w, r, getPageFromRequest(r))
	} else {
		tmpl, err := template.ParseFiles(getComp("app"))
		handleError(err)
		if err == nil {
			f, err := os.ReadFile(getPageFromRequest(r))
			handleError(err)
			if err == nil {
				tmpl.Execute(w, Template{template.HTML(f)})
			}	
		}
	}
}

func RenderComp(w http.ResponseWriter, r *http.Request) {
	folder := chi.URLParam(r, "folder")
	comp := chi.URLParam(r, "comp") 
	path :=  fmt.Sprintf("%v/components/%v/%v.html", pageDir, folder, comp)
	log.Println("Render comp:", path)

	http.ServeFile(w, r, path)
}