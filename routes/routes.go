package routes 

import (
	"server/pages"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

func NewRouter() *chi.Mux {
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Get("/leaderboards", pages.Render)
	r.Get("/getting-started", pages.Render)
	r.Get("/login", pages.Render)
	r.Get("/upload-run", pages.Render)
	r.Get("/comp/{folder}/{comp}", pages.RenderComp)
	r.Get("/", pages.Render)
	return r
}
