# Mario Kart Double Dash Leaderboards

## Core Functionality
- [ ] Leaderboard
    - [ ] View Leaderboard by Category
    - [ ] Upload Run to Leaderboard
- [ ] Basic Auth
- [ ] Display Meta

### Categories
- Race Type
    - Time Trials
    - Grand Prix
- Kart Speed
    - 50cc
    - 100cc
    - 150cc
    - Mirror
- Kart Loadout

### Upload Form
- Gamertag
- Country
- RTA time
- In-game time
- Video
- Kart loadout