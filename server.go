package main 

import (
	"net/http"
	"server/routes"
)

func main() {
	r := routes.NewRouter()
	http.ListenAndServe(":8080", r)
}