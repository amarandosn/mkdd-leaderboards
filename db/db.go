package db

type DB interface {
	NewUser (name string, url string) string
}

func NewDB(port string) DB {
	return &Scylla{port}
}